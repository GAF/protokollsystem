\ProvidesClass{ap_fssitzung}[2020/09/04 AP-Fachschaftssitzungs class by GAF]
\LoadClass[a4paper,parskip,fontsize=9pt,DIV=12]{scrartcl}

\RequirePackage{iftex}

\ifPDFTeX
			\RequirePackage[utf8]{inputenc}
			\RequirePackage[german]{babel}
			\RequirePackage[T1]{fontenc}
\else
	% Don't switch the order: github.com/reutenauer/polyglossia/issues/50
	\ifLuaTeX
			\RequirePackage[german]{babel}
			\RequirePackage[pdftex]{graphicx} % graphix always before hyperref
			\RequirePackage[pdftex,hidelinks]{hyperref}
	\else
		\ifXeTeX
			\RequirePackage{polyglossia}
			\setdefaultlanguage[babelshorthands=true]{german}
			\RequirePackage{xunicode} % loads graphicx so load it before hyperref
			\RequirePackage[xetex,hidelinks]{hyperref}
		\fi
	\fi
\fi


\RequirePackage{lmodern}
\RequirePackage{microtype}

\RequirePackage{ulem}
\normalem

\RequirePackage{url}
\RequirePackage{datatool}
\RequirePackage{xparse}
\RequirePackage{etoolbox}
\RequirePackage{ifthen}
\RequirePackage{collect}
\RequirePackage{wasysym}
\RequirePackage{marvosym}
\RequirePackage{xcolor}
\RequirePackage{mdframed}
\RequirePackage{dingbat}
\RequirePackage{enumitem}
\RequirePackage{graphicx}
\RequirePackage{fancyhdr}
\RequirePackage{geometry}
\RequirePackage[compact]{titlesec}
\RequirePackage{morewrites}
% Between tikz, databases, collections and exporting into text files: Ran out of writes
\RequirePackage{tikz} % For alternative pro/con







%% @TODO make @ version (which fails for now)
\NewDocumentCommand\beginn{m}{%
	\gdef\fssStart{#1}%
}

%% @TODO make @ version (which fails for now)
\NewDocumentCommand\ende{m}{%
	\gdef\fssEnd{#1}%
}


\def\defClearCollection#1{%
	\definecollection{#1}%
	\begin{collect}{#1}{}{}\end{collect}}


%% ## Internal/External ########################################################
%% ==== Detection ==============================================================
\newtoggle{fss@forExternal}

%% Only if the jobname is "internal" (xelatex -jobname internal ...), a version
%% with confidential informations is generated. This pessimistic logic hopefully
%% prevents some fails.
\expandafter\ifstrequal\expandafter{\jobname}{internal}%
{\togglefalse{fss@forExternal}}%
{\toggletrue{fss@forExternal}}


%% ==== User commands ==========================================================
%% For people who can't use jobnames, internal \sitzung command
%\NewDocumentCommand\sitzungInternal{m}{
%	\togglefalse{fss@forExternal}
%	\sitzung{#1}
%}

\NewDocumentCommand\intern{m}{%
	\iftoggle{fss@forExternal}{}{#1}%
}

\NewDocumentCommand\extern{m}{%
	\iftoggle{fss@forExternal}{#1}{}%
}

\NewDocumentCommand\internextern{mm}{%
	\iftoggle{fss@forExternal}{#2}{#1}%
}



%% ## Design ###################################################################
%% ==== Page ===================================================================
\geometry{
	includehead,
	includefoot,             %% vvv Margin        vvv Margin
	top=1cm,                 %% Upper page   <--> Upper head
	headsep=.8cm,            %% Lower head   <--> Upper text
	bottom=0cm,              %% Lower text   <--> Lower footer(!)
	footskip=1cm,            %% Lower footer <--> Lower page
	left=2.5cm,
	right=2.5cm,
	%showframe    %TODO REMOVE BEFORE PRODUCTION
}


%% ==== Titles =================================================================
%% Set the font style, put gray title numbers in the margin
\titleformat{\section}      {\normalfont\huge\sffamily\bfseries}{\llap{\textcolor{black!50}{\thesection}\hspace{1em}}}{0em}{}
\titleformat{\subsection}   {\normalfont\normalsize\sffamily\bfseries}{\llap{\textcolor{black!50}{\thesubsection}\hspace{1em}}}{0em}{}
\titleformat{\subsubsection}{\normalfont\small\sffamily\bfseries}{\llap{\textcolor{black!50}{\thesubsubsection}\hspace{1em}}}{0em}{}

%% Set left margin and the space above and below the title. The last two are
%% relative to the default "compact" style set with the titlesec package loading
%% options
\titlespacing{\section}{0cm}{*3}{*0}
\titlespacing{\subsection}{0cm}{*1}{*0}
\titlespacing{\subsubsection}{0pt}{*0}{*0}


%% ==== Definition lists =======================================================
%% This is later used as the alignment style of the definition list following
%% the title
\SetLabelAlign{parright}{\parbox[t]{\labelwidth}{\raggedleft#1}}


%% ==== Pro & Contra ===========================================================
\newmdenv[
	topline=false,
	bottomline=false,
	rightline=false,
	skipabove=1em,
	leftmargin=-.9em,
	innermargin=0pt,
	innertopmargin=0pt,
	innerbottommargin=0pt,
	innerleftmargin=0pt,
	linecolor=green!60,
	linewidth=2.5pt,
]{proBox}

\newmdenv[
	topline=false,
	bottomline=false,
	rightline=false,
	skipabove=1em,
	leftmargin=-.9em,
	innermargin=0pt,
	innertopmargin=0pt,
	innerbottommargin=0pt,
	innerleftmargin=0pt,
	linecolor=red!60,
	linewidth=2.5pt,
]{conBox}





%% ## Blocks with big & small veersion #########################################
%% ==== Block macro template ===================================================
%% Create a collection for all environment contents which have to be ignored
%% because of internal content in external versions
\defClearCollection{fss@ignore}

%% m: command macro name (german, all lowercase)
%% m: environment macro name (german, all caps)
%% m: end of the make macro name (english, first letter cap)
%% m: Title of all blocks (german)
%% m: Description if no block is defined in document (german)
%%    Competes "Keine $m."
\NewDocumentCommand\@fss@makeCommands{m m m m m}{
	\defClearCollection{fss@#1CollectionC}
	\defClearCollection{fss@#2CollectionE}
	
	\newtoggle{fss@#1FoundC}
	\newtoggle{fss@#2FoundE}
	
	%% User environment for the block
	%% s: Stared environment: Only for internal versions
	%% m: Title of the block
	\DeclareDocumentEnvironment{#2}{s m}{%
		\ifboolexpr{togl{fss@forExternal} and test{\IfBooleanTF ##1}}{
			%% For the external version of a stared environment, collect the
			%% environment content in the "fss@ignore" collection, which will
			%% never be used.
			\@nameuse{collect}{fss@ignore}{}{}
		}{
			\@nameuse{collect}{fss@#2CollectionE}{}{}
				\toggletrue{fss@#2FoundE}
				\subsection{##2}
		}
	}{% BODY
		\@nameuse{endcollect}
	}
	
	%% User command for the block
	%% s: Stared environment: Only for internal versions
	%% o: Title of the block
	%% m: Content of the block
	\expandafter\NewDocumentCommand\csname #1\endcsname{s o m}{
		\ifboolexpr{togl{fss@forExternal} and test{\IfBooleanTF ##1}}{
			%% Do nothing in external version with the stared command
		}{
			\IfNoValueTF{##2}{%
				%% If no title is given, collect an item for the list.
				\toggletrue{fss@#1FoundC}
				
				\begin{collect}{fss@#1CollectionC}{}{}
					\item ##3
				\end{collect}
			}{%
				%% If a title is given, use the environment of the block to
				%% create the "big" version.
				\begin{#2}{##2}
					##3
				\end{#2}
			}
		}
	}
	
	%% @TODO make @ version (which fails for now)
	\expandafter\NewDocumentCommand\csname make#3\endcsname{}{
		\section{#4}
		%
		%% Include the big version's collection (which can be empyt)
		\includecollection{fss@#2CollectionE}
		%
		\iftoggle{fss@#1FoundC}{%
			%% Include the small version's collection only if there are
			%% collected items. Otherwise there would be an empty list taking up
			%% space.
			%% 
			%% A own subsection for the small version's collection is only 
			%% needed on preciding the big version's collection.
			\iftoggle{fss@#2FoundE}{\subsection{Sonstiges}}{}
			%
			\begin{itemize}
				\includecollection{fss@#1CollectionC}
			\end{itemize}
		}{
			\iftoggle{fss@#2FoundE}{}{Keine #5.}
		}
	}
}


%% ==== Instantiate blocks =====================================================
%{<Name Befehl>}{<Name Umgebung>}{<makeName>}{<Überschrift>}{<Bezeichnung in Text>}
%% Creates \was, \begin{WAS}\end{WAS} and \makeWas
\@fss@makeCommands{was}{WAS}{Was}{W.A.S.}{W.A.S. Punkte}

%% Creates \post, \begin{POST}\end{POST} and \makeMail
\@fss@makeCommands{post}{POST}{Mail}{Post und E-Mails}{relevante Post}

%% Creates \diskussion, \begin{DISKUSSION}\end{DISKUSSION} and \makeDiscussions
\@fss@makeCommands{diskussion}{DISKUSSION}{Discussions}{Diskussion}{Diskussionen}

%% Creates \bericht, \begin{BERICHT}\end{BERICHT} and \makeReports
\@fss@makeCommands{bericht}{BERICHT}{Reports}{Berichte}{Berichte}


%% ==== Shortcuts ==============================================================
\NewDocumentCommand\berichtFakRat{m}{
	\bericht{Fakultätsrat #1}
}


%% ## Motion blocks ############################################################

\defClearCollection{fss@motionCollection}
\newtoggle{fss@motionFound}


\NewDocumentEnvironment{ANTRAG}{m m}{%
	\collect{fss@motionCollection}{%
		\subsection{#2}%
		\toggletrue{fss@motionFound}%
	}{% BODY
		\edef\status{#1}%
		%
		\ifdefstring{\status}{angenommen}{\par $\Rightarrow$ Der Antrag wurde einstimmig angenommen.}{%
		\ifdefstring{\status}{abgelehnt}{\par $\Rightarrow$ Für den Antrag konnte kein Konsens gefunden werden.}{%
			\PackageError{fssitzung}%
			{Das erste Argument von \\ANTRAG und \\antrag muss "angenommen" oder "abgelehnt" sein.}%
			{Das erste Argument von \\ANTRAG und \\antrag muss "angenommen" oder "abgelehnt" sein.}%
			\stop%
		}}%
	}%
}{% BODY
	\endcollect%
}

%% The small version is displayed exactly the same as the big version
\NewDocumentCommand\antrag{s m m G{}}{%
	\ifboolexpr{togl{fss@forExternal} and test{\IfBooleanTF #1}}{
		%% Do nothing in external version with the stared command
	}{
		\begin{ANTRAG}{#2}{\expandafter#3}#4\end{ANTRAG}%
	}
}

%% @TODO make @ version (which fails for now)
\NewDocumentCommand\makeMotions{}{
	\section{Anträge}
	\includecollection{fss@motionCollection}
	\iftoggle{fss@motionFound}{}{Keine Anträge.}
}

%% ==== Shortcuts ==============================================================
\NewDocumentCommand\transponderAntrag{m}{
	\antrag*{angenommen}{#1 bekommt einen Transponder}
}

\NewDocumentCommand\sesamAntrag{m}{
	\antrag*{angenommen}{#1 erhält Zugang zum Fachschaftsraum über
Sesam}
}

\NewDocumentCommand\accountAntrag{m}{
	\antrag*{angenommen}{#1 bekommt einen Account}
}


%%%%%%%%%%%%% Date %%%%%%%%%%%%%

\DTLnewdb{dateDb}
\DTLaddcolumn{dateDb}{when}

\NewDocumentCommand\termin{m m}{%
	\DTLnewrow{dateDb}%
	\DTLnewdbentry{dateDb}{what}{#2}%
	\DTLnewdbentry{dateDb}{when}{#1}%
}

\NewDocumentCommand\makeDates{}{
	\section{Anstehende Termine}
	
	\DTLifdbempty{dateDb}{
		Keine
	}{
		\begin{itemize}
			\DTLsort{when}{dateDb}%
			\DTLforeach*{dateDb}{\theWhat=what,\theWhen=when}{%
				\item \textbf{\theWhen}\\\theWhat
			}%
		\end{itemize}
	}
}


%%%%%%%%%%%%% Old ToDo %%%%%%%%%%%%%

\DTLnewdb{oldTodoDb}
\DTLaddcolumn{oldTodoDb}{done}
\DTLaddcolumn{oldTodoDb}{what}
\DTLaddcolumn{oldTodoDb}{status}
\DTLaddcolumn{oldTodoDb}{who}
\DTLaddcolumn{oldTodoDb}{until}

\NewDocumentCommand\alttodo{o m o d() o}{
	\DTLnewrow{oldTodoDb}%
	\DTLnewdbentry{oldTodoDb}{what}{#2}%
	\DTLnewdbentry{oldTodoDb}{who}{#4}%
	\IfNoValueTF{#1}{}{\DTLnewdbentry{oldTodoDb}{done}{#1}}%
	\IfNoValueTF{#3}{}{\DTLnewdbentry{oldTodoDb}{status}{#3}}%
	\IfNoValueTF{#5}{}{\DTLnewdbentry{oldTodoDb}{until}{#5}}%
}

\NewDocumentCommand\makeOldTodo{}{
	\section{Alte ToDo}
	
	\DTLifdbempty{oldTodoDb}{
		Keine
	}{
		\begin{itemize}
		\DTLsort{who,until}{oldTodoDb}
		\DTLforeach*{oldTodoDb}{\theDone=done,\theWhat=what,\theStatus=status,\theWho=who,\theUntil=until}{%
			\item[\DTLifnull{\theDone}{$\Box$}{\Checkedbox}] %
			\theWhat\intern{ (\theWho)}%
			\DTLifnull{\theStatus}{}{\\\emph{Status: \theStatus}}%
			\DTLifnull{\theUntil}{}{\\\emph{Bis: \theUntil}}%
			\DTLifnull{\theDone}{%
				\DTLifnull{\theUntil}{%
					\edef\foox{\todox{\theWhat}{\theWho}{\thesection}}%
				}{%
					\edef\foox{\todox{\theWhat}{\theWho}{\thesection}[\theUntil]}%
				}%
			}{%
				\DTLifnull{\theUntil}{%
					\edef\foox{\todox[\theDone]{\theWhat}{\theWho}{\thesection}}%
				}{%
					\edef\foox{\todox[\theDone]{\theWhat}{\theWho}{\thesection}[\theUntil]}%
				}%
			}%
			\foox%
		}%
		\end{itemize}
	}
}

%%%%%%%%%%%%% ToDo %%%%%%%%%%%%%

\DTLnewdb{todoDb}
\DTLaddcolumn{todoDb}{who}
\DTLaddcolumn{todoDb}{where}
\DTLaddcolumn{todoDb}{until}
\DTLaddcolumn{todoDb}{done}

\newwrite\todofile

\NewDocumentCommand\todo{o m d() o}{
	%\par\emph{\IfNoValueTF{#1}{ToDo: }{ToDo \checkmark: }}#2 (#3)%
	\par\emph{ToDo: }#2\intern{ (#3)} \IfNoValueTF{#1}{}{ \checkmark}%
	\IfNoValueTF{#4}{}{\\\emph{Bis #4}}%
	\edef\foox{\todox[#1]{#2}{#3}{\ifnumgreater{\value{subsection}}{0}{\thesubsection}{\thesection}}[#4]}%
	\foox%
}

\NewDocumentCommand\todox{ommmo}{
	\DTLnewrow{todoDb}%
	\DTLnewdbentry{todoDb}{what}{#2}%
	\DTLnewdbentry{todoDb}{who}{#3}%
	\DTLnewdbentry{todoDb}{where}{#4}%
	%
	%% Add an "until" flag in the ToDo DB & add a date
	\IfNoValueTF{#5}{}{%
		\DTLnewdbentry{todoDb}{until}{#5}%
		\IfNoValueTF{#1}{%
			\edef\barx{\termin{#5}{Deadline\intern{ für #3}: #2 (siehe #4)}}%
			\barx%
		}{}%
	}%
	%
	%% Add an "done" flag in the ToDo DB
	\IfNoValueTF{#1}{}{
		\expandafter\ifstrequal\expandafter{#1}{done}{}{%
			\PackageError{fssitzung}%
			{Das erste Argument von \\todo und \\alttodo muss "done" oder leer sein.}%
			{Das erste Argument von \\todo und \\alttodo muss "done" oder leer sein.}%
			\stop%
		}%
		\DTLnewdbentry{todoDb}{done}{#1}
	}%
}

%thanks, https://tex.stackexchange.com/a/47453
\newcommand\back{\@backslashchar}
\begingroup
\catcode`<=1 \catcode`>=2
\catcode`{=12 \catcode`}=12
\gdef\wbgroup<{>
\gdef\wegroup<}>
\endgroup

\NewDocumentCommand\makeTodo{}{
	\section{ToDo}
	
	\immediate\openout\todofile=next.tex
	
	\DTLifdbempty{todoDb}{
		Keine
	}{
		\DTLsort{who,where,until}{todoDb}
		\immediate\write\todofile{\back documentclass{fssitzung}}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back anwesend[leitung]{<Name>}}
		\immediate\write\todofile{\back anwesend[protokoll]{<Name>}}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back beginn{hh:mm}}
		\immediate\write\todofile{}
		\DTLforeach*{todoDb}{\theWhat=what,\theWho=who,\theWhere=where,\theUntil=until,\theDone=done}{%
			\DTLifnull{\theDone}{
				\ifdefequal{\lastWho}{\theWho}{}{
					\ifundef{\lastWho}{}{\end{itemize}}
					\subsection{\theWho}
					\edef\lastWho{\theWho}
					\begin{itemize}
				}
				\item[\DTLifnull{\theDone}{$\Box$}{\Checkedbox}] \theWhat\ (siehe \theWhere)%
				\DTLifnull{\theUntil}{}{\\\emph{Bis \theUntil}}%
				\DTLifnull{\theDone}{
					\immediate\write\todofile{%
						\back alttodo{\theWhat}(\theWho)%
						\DTLifnull{\theUntil}{}{[\theUntil]}}
				}{}
			}{}
		}%
		\ifundef{\lastWho}{}{\end{itemize}}
	
		\immediate\write\todofile{}
		\immediate\write\todofile{\back post[]{}}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back bericht[AKs]\wbgroup}
		\immediate\write\todofile{	\back textbf{AK Feste:}}
		\immediate\write\todofile{	\back textbf{AK GAF-Services:}}
		\immediate\write\todofile{	\back textbf{AK LuST:}}
		\immediate\write\todofile{	\back textbf{AK O-Phase:}}
		\immediate\write\todofile{	\back textbf{AK Root:}}
		\immediate\write\todofile{\wegroup}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back bericht[Konvent]{}}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back diskussion[thema]\wbgroup}
		\immediate\write\todofile{	Unterelemente:}
		\immediate\write\todofile{	\back todo{xyz}(Bernd Nörgel)}
		\immediate\write\todofile{	\back antrag{angenommen}{xyz}}
		\immediate\write\todofile{\wegroup}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back was[thema]{}}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back termin{yyyy-mm-dd}{nächste GAF-Sitzung}}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back ende{hh:mm}}
		\immediate\write\todofile{}
		\immediate\write\todofile{\back sitzung{dd.mm.yyyy}}
	}
	
	\immediate\closeout\todofile
}


%%%%%%%%%%%%% Pro/Con %%%%%%%%%%%%%

% http://ux.stackexchange.com/questions/36597/best-way-to-display-plus-minus-or-pros-cons-short-sentences

\newtoggle{@altProStyle}
\togglefalse{@altProStyle}
\newbool{insidePro}
\boolfalse{insidePro}
\newbool{insideCon}
\boolfalse{insideCon}

\NewDocumentCommand\switchProStyle{}{
  \iftoggle{@altProStyle}{
    \togglefalse{@altProStyle}
  }{
    \toggletrue{@altProStyle}
  }
}

\definecolor{proGreen}{RGB}{0,210,0}
\definecolor{conRed}{RGB}{230,0,0}

\newsavebox{\proPlus}
\begin{lrbox}{\proPlus}
	\tikz[baseline=-1.1ex] \path[fill=proGreen] (0,0) -- ++(0.8ex,0) -- ++(0,0.8ex) -- ++(0.4ex,0) -- ++(0,-0.8ex) -- ++(0.8ex,0) -- ++(0,-0.4ex) -- ++(-0.8ex,0) -- ++(0,-0.8ex) -- ++(-0.4ex,0) -- ++(0,0.8ex) -- ++(-0.8ex,0) -- ++(0,0.4ex);
\end{lrbox}

\newsavebox{\conMinus}
\begin{lrbox}{\conMinus}
	\tikz[baseline=-1.1ex] \path[fill=conRed] (0,0) -- ++(2.0ex,0) -- ++(0,-0.4ex) -- ++(-2.0ex,0) -- ++(0,0.4ex);
\end{lrbox}

\makeatletter
\NewDocumentCommand\pro{m}{
	\iftoggle{@altProStyle}{
	  \ifbool{insidePro}{}
	  {
	  	\begin{itemize}[labelsep = 6pt, leftmargin=16pt]
	  	\booltrue{insidePro}
	  }
	  
	  \item[\usebox{\proPlus}] #1
	  
	  \@ifnextchar\pro{}
	  {
	  	\end{itemize}
	  	\boolfalse{insidePro}
	  }
	}{
	  \ifbool{insidePro}
	  {}
	  {
	  	\begin{proBox}%
	  	\smash{%
	  		\makebox[0pt][r]{%
	  			\raisebox{-.69cm}%
	  			{\fcolorbox{green!60}{green!60}{\parbox[c][.5cm][c]{.5cm}{\color{black}\rightthumbsup}}\hspace{0pt}}%
	  		}%
	  	}%
	  	\begin{itemize}%
	  	\booltrue{insidePro}
	  }
	  
	  \item #1
	  
	  \@ifnextchar\pro
	  {}
	  {\end{itemize}\end{proBox}}
	}
}
\makeatother

\makeatletter
\NewDocumentCommand\con{m}{
	\iftoggle{@altProStyle}{
	  \ifbool{insideCon}{}
	  {
	  	\begin{itemize}[labelsep = 6.5pt, leftmargin=16pt]
	  	\booltrue{insideCon}
	  }
	  
	  \item[\usebox{\conMinus}] #1
	  
	  \@ifnextchar\con{}
	  {
	  	\end{itemize}
	  	\boolfalse{insideCon}
	  }
	}{
	  \ifbool{insideCon}
	  {}
	  {%
	  	\begin{conBox}%
	  	\smash{%
	  		\makebox[0pt][r]{%
	  			\raisebox{-.69cm}%
	  			{\fcolorbox{red!60}{red!60}{\parbox[c][.5cm][c]{.5cm}{\color{black}\rightthumbsdown}}\hspace{0pt}}%
	  		}%
	  	}%
	  	\begin{itemize}%
	  	\booltrue{insideCon}%
	  }%
	  %
	  \item #1
	  
	  \@ifnextchar\con
	  {}
	  {\end{itemize}\end{conBox}}
	}
}
\makeatother


%%%%%%%%%%%%% Anwesenheit %%%%%%%%%%%%%

\DTLnewdb{anwesendDb}
\DTLnewdb{modDb}
\DTLnewdb{protDb}

\DTLaddcolumn{anwesendDb}{name}
\DTLaddcolumn{anwesendDb}{from}
\DTLaddcolumn{anwesendDb}{to}
\DTLaddcolumn{modDb}{name}
\DTLaddcolumn{modDb}{from}
\DTLaddcolumn{modDb}{to}
\DTLaddcolumn{protDb}{name}
\DTLaddcolumn{protDb}{from}
\DTLaddcolumn{protDb}{to}

\NewDocumentCommand\anwesend{o d() m d()}
{
	\edef\type{#1}
	
	\IfNoValueTF{#1}{
		\def\dbName{anwesendDb}
	}{
		\ifdefstring{\type}{leitung}{\def\dbName{modDb}}{%
		\ifdefstring{\type}{protokoll}{\def\dbName{protDb}}{%
		\PackageError{fssitzung}{Ungueltige Rolle}{Erstes Argument bei \\anwesend muss "leitung", "prokotoll" oder nicht gesetzt sein.}\stop
	}}}
	
	\DTLnewrow{\dbName}
	
	\DTLnewdbentry{\dbName}{name}{#3}
	\IfNoValueTF{#2}{}{\DTLnewdbentry{\dbName}{from}{#2}}
	\IfNoValueTF{#4}{}{\DTLnewdbentry{\dbName}{to}{#4}}
}

\newcommand{\personlist}[1]{%
	\DTLsort{name}{#1}%
	%
	\DTLforeach*{#1}{\theName=name,\theFrom=from,\theTo=to}{%
		\DTLiffirstrow{}{\DTLiflastrow{ und}{,}} %
		\theName %
		\DTLifnull{\theFrom}{%
			\DTLifnull{\theTo}{}{ (bis {\theTo} Uhr)}%
		}{ %
			(ab \theFrom \DTLifnull{\theTo}{}{ bis \theTo} Uhr)%
		}%
	}%
}


%%%%%%%%%%%%% Dokumenterzeugung %%%%%%%%%%%%%
\NewDocumentCommand\sitzung{m}{
	\subtitle{\internextern{\emph{Internes}}{Öffentliches} Protokoll}
	\title{Außerplanmäßige Fachschaftssitzung}
	\date{\vspace{-1cm}#1}
	
	\makeatletter
	\fancyhf{}
	\fancyhead[L]{\internextern{Internes}{Öffentliches} Protokoll der außerplanmäßigen Fachschaftssitzung vom #1}
	\fancyhead[R]{Seite \thepage}
	\makeatother
	
	
	\pagestyle{fancy}
	\begin{document}
		\maketitle
		\thispagestyle{empty}
		
		\begin{description}[
			style=multiline,
			leftmargin=3cm,
			rightmargin=1cm,
			align=parright
		]
			        \item[Anwesend]       \internextern{\personlist{anwesendDb}}{\DTLrowcount{anwesendDb} Personen, Sitzungsleitung und Protokoll}
			\intern{\item[Sitzungleitung] \personlist{modDb}}
			\intern{\item[Protokoll]      \personlist{protDb}}
			        \item[Beginn]         \makeatletter\fssStart\makeatother
			        \item[Ende]           \makeatletter\fssEnd\makeatother
		\end{description}
		
		%% The first section starts traditionally with 0 (tzzz, computer
		%% scientists). Therefor we have to set section to -1
		\setcounter{section}{-1}
		\section{Sitzungsleitung und Protokoll}
		Die Vorschläge zu Sitzungsleitung und Protokollanten wurden angenommen.
		Das Protokoll der letzten Sitzung wurde fertiggestellt und angenommen.
		
% 		\makeMail
% 		\makeOldTodo
% 		\makeReports
		\makeDiscussions
 		\makeMotions
 		\makeWas
		\makeDates
%		\intern{\makeTodo}
	\end{document}
}

