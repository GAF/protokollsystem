Protokollsystem
===============

Externes Protokoll generieren: `xelatex filename`  
Internes Protokoll generieren: `xelatex -jobname internal filename`

Zur Benutzung der Vorlage muss diese offensichtlich auf dem Computer vorhanden sein und erkannt werden.
Die einfachste Methode hierzu ist es, sie in einen Ordner zusammen mit dem `.tex`-Dokument zu legen.

Nach erfolgreicher Erzeugung eines Dokuments, befindet sich im selben Ordner eine Datei namens `next.tex`.
Diese kann als Vorlage für das nächste Protokoll dienen und wird normalerweise mit dem letzten Protokoll zusammen verteilt.
Enthalten sind alle notwendigen Befehle sowie eine Liste der unvollendeten TODOs vom letzten Mal.

Nötige Befehle: Jedes Dokument muss beginnen mit `\documentclass{fssitzung}` (um die Vorlage zu benutzen) und enden mit `\sitzung{<datum>}` (Format gewöhnlich `dd.mm.yyyy`).
Außerdem müssen an beliebiger Stelle (aber zwischen den zuvor genannten Kommandos) `\beginn{<Uhrzeit>}` und `\ende{<Uhrzeit>}` enthalten sein.
Weiter muss mindestens eine Sitzungsleitung und ein Protokollant festgelegt sein (siehe anwesend).

Die Befehle der Vorlage sind in zwei Ebenen unterteilt.
Jeder Text oder Befehl der inneren Ebene muss in einem Befehl der äußeren Ebene stehen
und, von inneren Befehlen in äußeren abgesehen, sollten die Vorlagen-spezifischen Befehle nicht geschachtelt werden.

Das Protokoll wird per Default in einer externen Version erstellt, in der einige Informationen nicht angezeigt werden.
Eine interne Version wird erstellt, falls der jobname (d.h. der Name der erstellten pdf-Datei) "internal" ist.
Dies kann man z.B. durch `-jobname=internal` bei Kommandozeilenaufruf von LaTeX oder durch Umbenennen der `.tex`-Datei in `internal.tex` erreichen.

Für die Überschrift "Außerplanmäßige Fachschaftssitzung" wird in der .tex-Datei der Header 
```\documentclass{fssitzung}``` durch ```\documentclass{ap_fssitzung}``` ersetzt. Es werden die Punkte Mail, Alte ToDo, Berichte und ToDo weggelassen.

Anmerkung für Leute  mit Latex-Erfahrung: Nur ein Absatz pro Kommando, sonst Fehler.
Anmerkung für Leute ohne Latex-Erfahrung: Zeilenumbrüche innerhalb einzelner Kommandos mittels `\\`


Befehle der äußeren Ebene
-------------------------

```LaTeX
\beginn{<Uhrzeit>}
\ende{<Uhrzeit}
```
Jeweils genau einmal, an beliebiger Stelle.

```LaTeX
\sitzung{<datum>}
```
Nur einmal, muss letzter Befehl sein.

```LaTeX
\anwesend(<ab wann>){Name}(<bis wann>)
\anwesend[leitung](<ab wann>){Name}(<bis wann>)
\anwesend[protokoll](<ab wann>){Name}(<bis wann>)
```
Die Parameter in runden Klammern (ab/bis wann) sind optional.
Sollte für jeden Teilnehmer der Sitzung genau einmal an beliebiger Stelle im Protokoll vorhanden sein.
Jeweils mindestens ein Kommando `\anwesend` mit Parameter `leitung` bzw. `protokoll` muss vorhanden sein.

```LaTeX
\alttodo{<was?>}[<Status>](Wer?)
\alttodo[done]{<was?>}[<Status>](Wer?)
```
Für alte TODOs, sollte keinen inneren Befehl enthalten.
Der Parameter `[<Status>]` ist optional.

```LaTeX
\switchProStyle
```
Wechselt zwischen den Pro-Con-Styles.

```LaTeX
\was[<optional, Titel>]{<text>}
\begin{WAS}{Titel}
\end{WAS}
```
Das gleiche für `post`/`POST`, `diskussion`/`DISKUSSION`, `bericht`/`BERICHT`.
Beide Kommandos jedes Type machen im Prinzip das gleiche, erzeugt einen entsprechenden Punkt.
`\begin`-Blöcke müssen mit dem entsprechenden `\end`-Block geschlossen werden und dazwischen darf keine Zeile sein, die nicht mindestens ein `%` oder ein `\\` (Newline) enthält, sonst gibts eine obskure Fehlermeldung.
Jedes Kommando hat eine interne `*`-Version, z.B. `\was*[<titel]{<text>}` bzw. `\begin{WAS}*{<titel>} <text> \end{WAS}`,
die nur in der internen Version des Protokolls angezeigt wird.

```LaTeX
\berichtFakRat{<Nummer?>}
```
Kurz für `\bericht{Fakultätsrat <Nummer?>}`. Anmerkung: Fakrats-Berichte sind vertraulich und die Inhalte sollen nicht ins Protokoll übernommen werden.

```LaTeX
\antrag{<angenommen?>}{<Titel>}{<Text, optional>}
\begin{ANTRAG}{<angenommen?>}{<Titel>}
\end{ANTRAG}
```
Für Anträge, hat keine Stern-Version. `<angenommen?>` muss `angenommen` oder `abgelehnt` sein. Anträge können **nicht** in den W.A.S. eingefügt werden, weil sie im TeX (und eigentlich auch in der Sitzungsordnung) davor stehen und ansonsten beim Erstellen der PDF-Datei nicht mehr berücksichtigt werden.

```LaTeX
\transponderAntrag{<wer?>}
```
Kurz für `\antrag{angenommen}{<wer?> bekommt einen Transponder}`.

```LaTeX
\sesamAntrag{<wer?>}
```
Kurz für `\antrag{angenommen}{<wer?> erhält Zugang zum Fachschaftsraum
über Sesam}`.

```LaTeX
\accountAntrag{<wer?>}
```
Kurz für `\antrag{angenommen}{<wer?> bekommt einen Account}`.

```LaTeX
\termin{<datum>}{<was?>}
```
Datum idealerweise in Format `yyyy-mm-dd` (für korrekte Sortierung.)


Befehle der inneren Ebene
-------------------------

```LaTeX
\todo{<was?>}[<Status>](Wer?)
\todo{<was?>}[<Status>](Wer?)[<Deadline>]
\todo[done]{<was?>}[<Status>](Wer?)
\todo[done]{<was?>}[<Status>](Wer?)[<Deadline>]
```
Für TODOs, die Parameter `[<Status>]` und `[<Deadline>]` sind optional (und `<wer?>` semi-optional/erzeugt einen No-Value output, aber keinen Fehler). Wenn der Deadline-Parameter vorhanden und das TODO noch nicht erledigt ist, wird bei den Terminen ein Eintrag mit dem Wert des Parameters als Datumsangabe eingefügt.

```LaTeX
\pro{<Text>}
\con{<Text>}
```
Erzeugt einen Pro- bzw. Con-Punkt die Liste geht bis zum nächsten Text, der kein Pro oder Con ist.

```LaTeX
\intern{<Text>}
```
Text ist nur interner Version sichtbar.

```LaTeX
\extern{<Text>}
```
Text ist nur externer Version sichtbar.

```LaTeX
\internextern{<interner Text>}{<externer Text>}
```
Unterschiedlicher Text für verschiedene Versionen.

```LaTeX
\EUR{<Zahl>}
```
Schreibt im Protokoll "€ <Zahl>".
